<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}



/** @global $APPLICATION */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.js');

Asset::getInstance()->addString('<link rel="icon" type="image/x-icon" href="' . SITE_TEMPLATE_PATH . '/favicon.ico"/>');
Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');
?>
<!doctype html>
<html xml:lang="<?php echo LANGUAGE_ID ?>" lang="<?php echo LANGUAGE_ID ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta class="js-meta-viewport" name="viewport" content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <title>Yanicode :: Главная</title>
    <link rel="stylesheet" href="/local/templates/yanicode/assets/template_styles.css">
    <title><?php echo $APPLICATION->ShowTitle() ?></title>
    <?php $APPLICATION->ShowHead(); ?>
</head>
<body>
<div id="panel"><?php $APPLICATION->ShowPanel() ?></div>
<header class="header">
    <div class="container">
        <div class="header-wrapper">
            <a href="../../../index.php" class="header__logo">
                <img width="300" height="92" src="/local/templates/yanicode/assets/images/svg/logo-yanicode.svg" alt="yanicode">
            </a>
            <div class="header__burger header__burger_close">
                <span class="burger-line"></span>
                <span class="burger-line"></span>
                <span class="burger-line"></span>
            </div>
            <div class="header-nav">
                <nav class="nav-list">
                    <a href="../../../index.php" class="nav-list__item">ГЛАВНАЯ</a>
                    <a href="services.php" class="nav-list__item">УСЛУГИ</a>
                </nav>
                <div class="header__phone">
                    <a href="tel:+79114510616">+79114510616</a>
                </div>
            </div>
        </div>
    </div>
</header>

</body>
</html>
